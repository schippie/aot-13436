import { enableProdMode } from "@angular/core";
import { platformBrowser }    from '@angular/platform-browser';

import { AppModuleNgFactory } from '../aot/src/module.ngfactory';

enableProdMode();
platformBrowser().bootstrapModuleFactory(AppModuleNgFactory);