import { NgModule } from "@angular/core";
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from "@angular/http";

import { AppComponent } from './component';
import { CoreModule } from "./core";

/**
 * We bootstrap or application here. We only add our CUSTOM PROVIDERS into the bootstrap to keep this class nice and clean
 * If we require other providers please add those to the array of providers within ./injectables/providers/ and not here!
 */
@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
	    HttpModule,
        CoreModule
    ],
	declarations: [AppComponent],
	bootstrap: [AppComponent]
})
export class AppModule {}