import { Component } from '@angular/core';
import { UserResource, PlayersState, PlayerResource } from './core';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'dummy-app',
    template: `
       <div *ngFor="let player of (players | async)?.toArray()">
       		{{player.userName}}	
       </div>
    `
})
export class AppComponent {
	players: Observable<PlayersState>;

    constructor(
        private userResource: UserResource,
		private playerResource: PlayerResource
	) {
        this.players = this.playerResource.get();
    }
}