import { UserResource } from "./user";
import { PlayerResource } from "./player";

export const RESOURCE_PROVIDERS = [
	UserResource,
	PlayerResource
];