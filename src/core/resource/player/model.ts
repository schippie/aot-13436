import { Map, Record } from "immutable";

export class PlayerActions {}

export interface PlayersState extends Map<number, Player> {}

export interface PlayerState {
    userName: string
    firstName: string
    surName: string
}

export class Player extends Record({
    userName: '',
    firstName: '',
    surName: ''
}) implements PlayerState {
    userName: string;
    firstName: string;
    surName: string;
}

export const initialPlayersState: PlayersState = <PlayersState>Map();