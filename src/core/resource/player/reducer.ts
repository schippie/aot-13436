import { ActionReducer, Action } from "@ngrx/store";
import { PlayersState, Player, PlayerActions, initialPlayersState } from "./model";

export const players: ActionReducer<PlayersState> = (state: PlayersState = initialPlayersState, action: Action) => {
	switch(action.type) {
		default:
			return state;
	}
};