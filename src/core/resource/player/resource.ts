import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Observable";

import { ApplicationState } from "../store";
import { PlayersState } from "./model";

@Injectable()
export class PlayerResource {
	private players: Observable<PlayersState>;
	
	constructor(private store: Store<ApplicationState>) {
		this.players = this.store.select<PlayersState>('players');
	}
	
	get(): Observable<PlayersState> {
		return this.players;
	}
}