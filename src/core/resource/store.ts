import { StoreModule, combineReducers } from "@ngrx/store";
import { UserState, user } from "./user";
import { PlayersState, players } from "./player";

export interface ApplicationState {
    user: UserState
    players: PlayersState
}

const reducers = {
	user,
	players
};

const appState = combineReducers(reducers);

export function reducer(state: any, action: any) {
	return appState(state, action);
}

export const applicationStore = StoreModule.provideStore(reducer);