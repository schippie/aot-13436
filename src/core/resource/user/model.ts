import { Record } from "immutable";

export class UserActions {}

export interface UserState {
    userName: string
    firstName: string
    surName: string
}

export class User extends Record({
    userName: "",
    firstName: "",
    surName: ""
}) implements UserState {
    userName: string;
    firstName: string;
    surName: string;
}

export let initialUserState: User = new User();