import { ActionReducer, Action } from "@ngrx/store";
import { User, UserActions, initialUserState } from "./model";

export const user: ActionReducer<any> = (state: User = initialUserState, action: Action) => {
	switch(action.type) {
		default:
			return state;
	}
};