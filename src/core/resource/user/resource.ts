import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Observable";

import { ApplicationState } from "../store";
import { User } from "./model";

@Injectable()
export class UserResource {
	private user: Observable<User>;

	constructor(private store: Store<ApplicationState>) {
		this.user = this.store.select<User>('user');
	}
	
	get(): Observable<User> {
		return this.user;
	}
}