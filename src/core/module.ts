import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RESOURCE_PROVIDERS, applicationStore } from "./resource";

@NgModule({
	imports: [CommonModule, applicationStore],
	providers: [
		...RESOURCE_PROVIDERS
	]
})
export class CoreModule {}
