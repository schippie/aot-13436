/**
 * Created by Nick Schipper - Twensoc:
 * @author : Twensoc
 * Date: 12/20/2016
 * Time: 13:34
 * For Project: rmgapp
 */

var fs = require('fs-extra');

/**
 * Empty the tmp directory.
 */
fs.emptyDirSync('./tmp', function(error) {
	if(!error) console.log('Successfully emptied tmp directory');
});

fs.emptyDirSync('./aot', function(error) {
	if(!error) console.log('Successfully emptied aot directory');
});