/**
 * Created by Nick Schipper - Twensoc:
 * @author : Twensoc
 * Date: 12/19/2016
 * Time: 12:30
 * For Project: rmgapp
 */

import rollup      from 'rollup'
import nodeResolve from 'rollup-plugin-node-resolve'
import commonjs    from 'rollup-plugin-commonjs';
import uglify      from 'rollup-plugin-uglify';
import git         from 'git-rev-sync';

export default {
	entry: 'tmp/src/main-aot.js',
	dest: 'dist/' +  git.tag() + '/build.js', // output a single application bundle
	sourceMap: true,
	format: 'iife',
	moduleName: 'dummy',
	plugins: [
		nodeResolve({
			jsnext: true,
			module: true
		}),
		commonjs({
			include: 'node_modules/**',
			namedExports: {
				'node_modules/immutable/dist/immutable.js': ['Record', 'Map', 'Set', 'Collection', 'List', 'fromJS']
			}
		}),
		uglify()
	]
}